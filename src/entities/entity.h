#ifndef ENTITY_HPP
#define ENTITY_HPP

#include <SFML/Graphics/Sprite.hpp>

class Gamescreen;


class Entity: public sf::Sprite
{
public:
	Entity();
	virtual ~Entity();
	void setManager(Gamescreen* parent);
	Gamescreen* getManager() const;
	bool isAlive() const;
	void setAlive();
	bool collidesWith(const Entity& ent) const;
	virtual void update(float frametime) = 0;
	virtual void creatureHit(Entity& creature) {}
	virtual void objectHit(Entity& object) {}
    virtual void powerupHit(Entity& powerup) {}
    virtual void towerHit(Entity& tower) {}
	virtual void groundHit() {}
	virtual void wallHit() {}
	inline float getX() const { return getPosition().x; }
	inline float getY() const { return getPosition().y; }
	inline void setX(float x) { setPosition(x, getPosition().y); }
	inline void setY(float y) { setPosition(getPosition().x, y); }
	float getWidth() const;
	float getHeight() const;
	sf::IntRect getCollisionRect() const;

private:
	Gamescreen* m_parent;
	bool m_alive;
};

#endif
