#include<cmath>
#include <math.h>
#include <SFML/System/Vector2.hpp>
#include <cstdlib>

namespace math{

const double PI = M_PI;

inline double toRad(double degrees){
	return degrees * PI / 180;
}
inline double toDeg(double radians){
	int degrees = (round(radians / PI * 180)); // normaliseren
	return degrees < 0 ? degrees + 360 : degrees;
}
inline float distance(const sf::Vector2f& a, const sf::Vector2f& b = sf::Vector2f(0.f, 0.f))
{
	// sqrt((x1 - x2)² + (y1 - y2)²)
	return std::sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
}
inline float angle(const sf::Vector2f& a, const sf::Vector2f& b = sf::Vector2f(0.f, 0.f))
{
	// Y-axis is flipped
	return std::atan2(b.y - a.y, b.x - a.x);
}
template <class T>
inline T clamp(T value, T min, T max)
{
	return value < min ? min : (value > max ? max : value);
}

}
