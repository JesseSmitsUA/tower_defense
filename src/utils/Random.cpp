#include <cmath>
#include <cstdlib>
#include <ctime>


class Random
{
    private:
        Random() // constructor is private so it cant be called form the outside
        {
            srand (time(NULL));
        }
    public:
        static Random& instance()  // only 1 object can be created ( meaning of singleton class )
        {
            static Random instance;

            return instance;
        }

        int randI(int begin, int end)
        {
        	return std::rand() % (end - begin + 1) + begin;
        }


        float randF(float begin, float end)
        {
        	return static_cast<float>(std::rand()) / RAND_MAX * (end - begin) + begin;
        }
};
